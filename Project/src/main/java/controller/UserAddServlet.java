package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("LoginServlet");
      return;


    }

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);


  }



  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {



    request.setCharacterEncoding("UTF-8");



    String userloginid = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String passwordconfirm = request.getParameter("password-confirm");
    String name2 = request.getParameter("name");
    String birthdate = request.getParameter("birth-date");


    UserDao userDao = new UserDao();

    if (userloginid.equals("") || !password.equals(passwordconfirm) || password.equals("")
        || name2.equals("") || birthdate.equals("") || !userDao.isLoginIdSame(userloginid)) {

      request.setAttribute("errMsg", "入力された内容は正しくありません");
      request.setAttribute("userloginid", userloginid);
      request.setAttribute("humanname", name2);
      request.setAttribute("humanbirthdate", birthdate);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;

    } else {

      String encodepass = utill.PasswordEncorder.SecretPassword(password);


      userDao.insert(userloginid, encodepass, name2, birthdate);

      response.sendRedirect("UserListServlet");



    }



  }

}
