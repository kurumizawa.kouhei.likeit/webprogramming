package controller;

import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("LoginServlet");
      return;


    }

    String idString = request.getParameter("id");


    int id = Integer.valueOf(idString);


    System.out.println(id);



    UserDao userDao = new UserDao();

    User user = userDao.findById(id);
    request.setAttribute("user", user);



    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
    return;
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub


    request.setCharacterEncoding("UTF-8");


    String password = request.getParameter("password");
    String passwordconfirm = request.getParameter("password-confirm");
    String userName = request.getParameter("user-name");
    String birthdate = request.getParameter("birth-date");
    String id = request.getParameter("user-id");
    String userloginid = request.getParameter("userLoginId");


    if (password.equals("") && passwordconfirm.equals("")) {





      UserDao userDao = new UserDao();
      userDao.updatePass(userName, birthdate, id);

      response.sendRedirect("UserListServlet");



    }


    else if (userName.equals("") || passwordconfirm.equals("") || !password.equals(passwordconfirm)
        || birthdate.equals("")) {


      // SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd");
      // date = birthdate.equals("") ? null : sdFormat.parse(birthdate);
      Date date = null;
      if (!birthdate.equals("")) {
        date = java.sql.Date.valueOf(birthdate);
      }

      User user = new User();
      user.setLoginId(userloginid);
      user.setName(userName);
      user.setBirthDate(date);

      request.setAttribute("errMsg", "入力された内容は正しくありません");
      request.setAttribute("user", user);



      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;

    } else {
      String encodepass = utill.PasswordEncorder.SecretPassword(password);



      UserDao userDao = new UserDao();
      userDao.update(encodepass, userName, birthdate, id);

      response.sendRedirect("UserListServlet");
    }



  }

}
