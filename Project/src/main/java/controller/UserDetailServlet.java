package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserDetailServlet */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserDetailServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("LoginServlet");
      return;


    }



    // URLからGETパラメータとしてIDを受け取る
    String idString = request.getParameter("id");
    // IntegerクラスのValueOfメソッド使う
    int id = Integer.valueOf(idString);

    // 確認用：idをコンソールに出力
    System.out.println(id);

    // TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する

    UserDao userDao = new UserDao();

    User user = userDao.findById(id);
    request.setAttribute("user", user);

    if (user == null) {


      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }


    // TODO 未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
    dispatcher.forward(request, response);


  }
}
