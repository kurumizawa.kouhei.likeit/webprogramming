package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDeleteServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");
    
    if (userInfo == null) {
      
      response.sendRedirect("LoginServlet");
      return;


    }


    String idString = request.getParameter("id");

    // IntegerクラスのValueOfメソッド使う
    int id = Integer.valueOf(idString);

    // 確認用：idをコンソールに出力
    System.out.println(id);

    // TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する

    UserDao userDao = new UserDao();

    User user = userDao.findById(id);
    request.setAttribute("user", user);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
    dispatcher.forward(request, response);


  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");



    String id = request.getParameter("user-id");



    UserDao userDao = new UserDao();
    userDao.delete(id);

    response.sendRedirect("UserListServlet");

  }

}
