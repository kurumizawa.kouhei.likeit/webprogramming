package utill;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {
  public static String SecretPassword(String secret) {



    // 変換したい文字列を入力

    // ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    // ハッシュ関数の種類(今回はMD5)
    String algorithm = "MD5";

    String encodeStr = "";

    // ハッシュ生成処理
    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(secret.getBytes(charset));

      encodeStr = DatatypeConverter.printHexBinary(bytes);

      // 暗号化結果の出力
      System.out.println(encodeStr);

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }

    return encodeStr;

  }

}
