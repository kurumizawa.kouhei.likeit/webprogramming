package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      System.out.println(loginId + " " + password);

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String loginIdData = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String passwordData = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, loginIdData, name, birthDate, passwordData, isAdmin, createDate,
          updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void insert(String userloginid, String password, String name2, String birthdate) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) values(?,?,?,?,now(),now()) ";


      System.out.println(userloginid + " " + password + " " + name2 + " " + birthdate);

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userloginid);
      pStmt.setString(2, name2);
      pStmt.setString(3, birthdate);
      pStmt.setString(4, password);


      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }

  }

  public User findById(int id) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      String sql = "select * from user where id = ? ";

      System.out.println(id);

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setLong(1, id);
      ResultSet rs = pStmt.executeQuery();

      rs.next();
      int id1 = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id1, loginId, name, birthDate, password, isAdmin, createDate, updateDate);



    } catch (SQLException e) {

      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return null;

  }

  public void update(String password, String userName, String birthdate, String id) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "UPDATE user SET password = ? , name = ? , birth_date = ?, update_date = now() WHERE id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthdate);
      pStmt.setString(4, id);



      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }


  }

  public void delete(String id) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = " DELETE FROM user WHERE id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, id);

      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }


  public boolean isLoginIdSame(String loginid) {

    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = " SELECT * FROM user WHERE login_id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, loginid);

      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return true;
      }


    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
    return false;

  }

  public List<User> Search(String userloginid, String username, String datestart, String dateend) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();


    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE is_admin = false");
      // String sql =
      // "SELECT * FROM user WHERE login_id = ? and name LIKE %?% and datestart <= ? and dateend >=
      // ? ";

      System.out.println(userloginid + " " + username + " " + datestart + " " + dateend);
      ArrayList<String> sqlList = new ArrayList<String>();


      if (!userloginid.equals("")) {

        sql.append(" AND login_id = ? ");
        sqlList.add(userloginid);


      }
      if (!username.equals("")) {

        sql.append(" AND name LIKE ? ");
        sqlList.add("%" + username + "%");

      }
      if (!datestart.equals("")) {

        sql.append(" AND birth_date >= ? ");
        sqlList.add(datestart);

      }
      if (!dateend.equals("")) {

        sql.append(" AND birth_date <= ?");
        sqlList.add(dateend);

      }


      PreparedStatement pStmt = conn.prepareStatement(sql.toString());
      for (int i = 0; i < sqlList.size(); i++) {

        pStmt.setString(i + 1, sqlList.get(i));
      }



      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void updatePass(String userName, String birthdate, String id) {
   
    Connection conn = null;
    try {

      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "UPDATE user SET   name = ? , birth_date = ?, update_date = now() WHERE id = ? ";



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
     
      pStmt.setString(1, userName);
      pStmt.setString(2, birthdate);
      pStmt.setString(3, id);



      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }


  }
    
  }






