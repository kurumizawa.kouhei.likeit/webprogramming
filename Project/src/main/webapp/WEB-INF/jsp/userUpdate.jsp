<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ更新画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>

	<!-- header -->
	<header>
		<nav
			class="navbar navbar-dark bg-dark navbar-expand  flex-md-row justify-content-end">

			<ul class="navbar-nav flex-row">
				<li class="nav-item"><span class="navbar-text">
						${userInfo.name}さん</span></li>
				<li class="nav-item"><a class="nav-link text-danger"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>

	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ情報更新</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<form action="UserUpdateServlet" method="post">


					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>


					<input type="hidden" name="user-id" value="${user.id}">
					<div class="form-group row">
					<input type="hidden" name="userLoginId" value="${user.loginId}">
						<label for="loginId" class="col-3 col-form-label">ログインID</label>
						<div class="col-9">${user.loginId}</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-3 col-form-label">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" class="form-control"
								id="password">
						</div>
					</div>

					<div class="form-group row">
						<label for="password-comfirm" class="col-3 col-form-label">パスワード(確認)</label>
						<div class="col-9">
							<input type="password" name="password-confirm"
								class="form-control" id="password-comfirm">
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-3 col-form-label">ユーザ名</label>
						<div class="col-9">
							<input type="text" name="user-name" class="form-control"
								id="userName" value="${user.name}">
						</div>
					</div>

					<div class="form-group row">
						<label for="birthDate" class="col-3 col-form-label">生年月日</label>
						<div class="col-9">
							<input type="date" name="birth-date" class="form-control"
								id="birth-date" value="${user.birthDate}">
						</div>
					</div>

					<div class="row">
						<div class="col">
							<button type="submit" class="btn btn-primary btn-block">更新</button>
						</div>
					</div>
				</form>


				<div class="row mt-3">
					<div class="col">
						<a href="UserListServlet">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>





</body>

</html>